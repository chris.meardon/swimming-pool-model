import numpy as np
import matplotlib.pyplot as plt
import random
import math


def add_pool_sides(array, pool_width, pool_length):
    for i in range(pool_width):
        array[0][i] = 1
        array[-1][i] = 1
    for i in range(pool_length):
        array[i][0] = 1
        array[i][-1] = 1
    return array


def print_pool(pool_array):
    """
    heat map of pool_array 
    """
    plt.imshow(pool_array, cmap="OrRd")
    plt.colorbar()
    plt.show()


def create_pool(**kwargs):
    pool_length = kwargs.get("pool_length", 25)  # in meters
    pool_width = kwargs.get("pool_width", 10)  # in meters
    point_density = kwargs.get("point_density", 1)  # in points per meter

    points_width = pool_width * point_density
    points_length = pool_length * point_density

    array = np.zeros([points_length, points_width])
    pool = add_pool_sides(array, points_width, points_length)
    return pool


def create_swimmers(num):
    """
    creates a list of num amount of swimmers giving them each a unique id and a speed of 2. 
    """
    swimmers = []
    for i in range(num):
        swimmers.append(
            {"id": i, "speed": 2, "direction": 0, "position": 0, "radius": 1}
        )
    return swimmers


def place_swimmers(swimmers, points_width, points_length):
    """
    takes a list of swimmers and gives them a random but unique position in the pool array
    """
    for swimmer in swimmers:
        for swimmer_ in swimmers:
            try:
                if (
                    swimmer["position"][0] == swimmer_["position"][0]
                    and swimmer["position"][1] == swimmer_["position"][1]
                ):
                    swimmer["position"] = [
                        random.randint(0, pool_width),
                        random.randint(0, pool_width),
                    ]
                else:
                    break

            except IndexError:
                swimmer["position"] = [
                    random.randint(0, pool_width),
                    random.randint(0, pool_width),
                ]
            except TypeError:
                swimmer["position"] = [
                    random.randint(0, pool_width),
                    random.randint(0, pool_width),
                ]

    return swimmers


def collision_find(swimmer, swimmers):
    """
    finds the time of collision for swimmer and the closest other swimmer in swimmers
    """
    collision_times = []
    for swimmer_ in swimmers:
        if swimmer == swimmer_:
            pass
        else:
            # swimmer is swimmer 1 and swimmer_ is swimmer2
            # direction is the clockwise angle from upwards y
            v1 = swimmer["speed"]
            a1 = math.sin(math.degrees(swimmer["direction"]))
            x10 = swimmer["position"][0]
            y10 = swimmer["position"][1]
            R1 = swimmer["radius"]

            v2 = swimmer_["speed"]
            a2 = math.sin(math.degrees(swimmer_["direction"]))
            x20 = swimmer_["position"][0]
            y20 = swimmer["position"][1]
            R2 = swimmer["radius"]

            A = v1*a1 - v2*a2

            min_x_t = (x20 - x10 - R1 - R2)/A
            max_x_t = (x20 - x10 + R1 + R2)/A

            min_y_t = (y20 - y10 - R1 - R2)/A
            max_y_t = (y20 - y10 + R1 + R2)/A
            
            if min_x_t < min_y_t and max_x_t < min_y_t:
                pass
            elif min_x_t > min_y_t and max_x_t > max_y_t: 
                pass
            else:
                


            collision_times.append(collision_time_x, swimmer_["id"])

    collision_time = min(collision_times)
    colliding_swimmer = collision_times[collision_times.index(collision_time)][
        2
    ]
    return (collision_time, colliding_swimmer)


def repulsion(swimmer, swimmers, pool):
    """
    changes the direction of the swimmer based on the pool and other swimmers
    """
    for swimmer in swimmers:
        dir_change = (
            1
        )  # add some function for finding the change in direction here

        swimmer["direction"] = swimmer["direction"] + dir_change

    return swimmers


def time_evolve(swimmers, dt):
    """
    change swimmers position and direction for one time step
    """
    for swimmer in swimmers:
        swimmer


pool_width = 10
pool_length = 25
point_density = 1

points_width = pool_width * point_density
points_length = pool_length * point_density


pool = create_pool(
    pool_width=pool_width, pool_length=pool_length, point_density=point_density
)
print(pool)

swimmers = place_swimmers(create_swimmers(4), points_width, points_length)
