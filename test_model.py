import pytest
<<<<<<< HEAD
from model import add_pool_sides,create_swimmers, place_swimmers
import numpy as np
import pprint

def test_add_pool_sides():

    array = add_pool_sides(np.zeros([3,3]),3,3)
    assert array[0,0] == 1
    assert array[-1,-1] == 1
    assert len(array[0]) == 3
    assert array[1,1] == 0


def test_create_swimmers():
    swimmers = create_swimmers(1)
    assert len(swimmers) == 1
    swimmers = create_swimmers(10)
    assert len(swimmers) == 10
    assert swimmers[-1]["id"] == 9

def test_place_swimmers():
    swimmers = [{'position':0}]*1000
    points_width = 100
    points_length = 100

    assert place_swimmers(swimmers,points_width,points_length)
=======
from model import *


def test_update_swim_position():
    swimmer = update_swim_position(
        [{"id": 1, "speed": 2, "direction": 0, "position": {"x": 0, "y": 0}}], 1
    )
    swimmer = swimmer[0]
    assert swimmer["position"]["x"] == 2
    assert swimmer["position"]["y"] == 0
    assert swimmer["speed"] == 2
    assert swimmer["direction"] == 0

def test_find_closest_x():
    a = {"id":0,"position": {"x": 0, "y": 0}}
    b = {"id":1,"position": {"x": 1.1, "y": 0}}
    c = {"id":2,"position": {"x": 2, "y": 0}}
    d = {"id":3,"position": {"x": 3, "y": 0}}

    swimmers = [a,b,c,d]

    swimmers = find_closest(swimmers)

    assert 1 == swimmers[0]["closest"]["id"]
    assert 2 == swimmers[1]["closest"]["id"]
    assert 1 == swimmers[2]["closest"]["id"]
    assert 2 == swimmers[3]["closest"]["id"]

def test_find_closest_y():
    a = {"id":0,"position": {"x": 0, "y": 0}}
    b = {"id":1,"position": {"x": 0, "y": 1.1}}
    c = {"id":2,"position": {"x": 0, "y": 2}}
    d = {"id":3,"position": {"x": 0, "y": 3}}

    swimmers = [a,b,c,d]

    swimmers = find_closest(swimmers)

    assert 1 == swimmers[0]["closest"]["id"]
    assert 2 == swimmers[1]["closest"]["id"]
    assert 1 == swimmers[2]["closest"]["id"]
    assert 2 == swimmers[3]["closest"]["id"]

def test_find_closest_x_and_y():
    a = {"id":0,"position": {"x": 0, "y": 0}}
    b = {"id":1,"position": {"x": 1.1, "y": 1.1}}
    c = {"id":2,"position": {"x": 2, "y": 2}}
    d = {"id":3,"position": {"x": 3, "y": 3}}

    swimmers = [a,b,c,d]

    swimmers = find_closest(swimmers)

    assert 1 == swimmers[0]["closest"]["id"]
    assert 2 == swimmers[1]["closest"]["id"]
    assert 1 == swimmers[2]["closest"]["id"]
    assert 2 == swimmers[3]["closest"]["id"]
    

def test_find_distance():
    a = {"position": {"x": 0, "y": 0}}
    b = {"position": {"x": 3, "y": 4}}
    assert 5 == find_distance(a, b)
>>>>>>> 9e29678a7ea3b4f81556a04c3250414dea5c8309
